# README

* Ruby version
ruby 2.2.4


## To run the app:
enter in terminal/cmd:
```
bundle install
rake db:create
rake db:migrate
rake db:seed 
rake scraper:all
rails s
```

### To run scheduler, open another terminal/cmd...
enter in terminal/cmd:
`bundle exec crono RAILS_ENV=development`


## API Spec
* To get location ID and details
`GET weather/locations`

* data of a specific location given a date
`GET weather/data/:location_id/:date`

* data of a postcode given a date
`GET weather/data/:postcode/:date`

* prediction within the postcode area and given a period(time interval)
`GET weather/prediction/:postcode/:period`

* prediction with latitude and longitude and given a period(time interval)
`GET weather/prediction/:lat/:long/:period`
