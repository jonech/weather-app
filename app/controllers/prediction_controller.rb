class PredictionController < ApplicationController
  def predict_by_lat_long_period
    lat = params[:lat].to_f
    long = params[:long].to_f
    @period = params[:period].to_i
    time = Time.now
    s = search_near_station(lat,long)
    s.each do |station|
      Rake::Task['scrape:bomu'].execute(station.name)
      Rake::Task['scrape:forecastu'].execute(station.name)
    end
    @predict_json = {"lattitude" => lat,
                    "longitude" => long,
                    "predictions" => {
                      "0" => predict_by_time(s,time,0),
                      @period => predict_by_time(s,time,@period)
                      }
                    }
    respond_to do |format|
      format.html
      format.json {render json: @predict_json.to_json}
    end
  end

  def predict_by_postcode_period
    @postcode = params[:postcode].to_i
    @period = params[:period].to_i
    time = Time.now
    s = included_station(@postcode)
    s.each do |station|
      Rake::Task['scrape:bomu'].execute(station.name)
      Rake::Task['scrape:forecastu'].execute(station.name)
    end
    @predict_json = {"postcode" => @postcode,
                    "predictions" => {
                      "0" => predict_by_time(s,time,0),
                      @period => predict_by_time(s,time,@period)
                      }
                    }
    respond_to do |format|
      format.html
      format.json {render json: @predict_json.to_json}
    end
  end

  private

  def time_transfer(time)
    t = ((time - Time.now) / 60).to_i + 8 * 24 * 60
    return t
  end

  def included_station(postcode)
    result = []
    valid = Station.select {|p| p.postcode == postcode}
    if(valid.length > 0)
    	result += valid
    end
    pcode = postcode
    while result.length < 3
      if pcode < 3500
        while pcode < 4000 do
          pcode += 1
          valid = Station.select {|p| p.postcode == pcode}
          if(valid.length > 0)
            result += valid
            break
          end
        end
      else
        while pcode >= 3000 do
          pcode -= 1
          valid = Station.select {|p| p.postcode == pcode}
          if(valid.length > 0)
            result += valid
            break
          end
        end
      end
    end
    puts result
    return result
  end

  def search_near_station(lat,long)
    stations = []
    distances = []
    @stations = Station.all
    @stations.each do |station|
      distances << station.get_distance(lat,long)
    end
    while stations.length < 3 do
      i = distances.each_with_index.min[1]
      stations << @stations[i]
      distances[i] = 1000000
    end
    return stations
  end

  def predict_precip_data(stations,time,period)
    precips = []
    stations.each do |station|
      times = []
      precip = []
      @valid = station.readerbs.select {|p| p.time.to_date == time.to_date}
      @valid.each do |p|
        if (p.rain && p.rain.amount)
          times << time_transfer(p.time)
          precip << p.rain.amount
        end
      end
      @valid = station.readers.select {|p| p.time.to_date == time.to_date}
      @valid.each do |p|
        if (p.rain && p.rain.amount)
          times << time_transfer(p.time)
          precip << p.rain.amount
        end
      end
      precips << [times,precip]
    end
    @prediction = Prediction.new()
    h = {0 => 1, 10 => 0.9, 30 => 0.8, 60 => 0.7, 120 => 0.6, 180 => 0.5}
    precipitation = @prediction.get_regress(time_transfer(time)+period,precips,40)
    precipitation_json = { "precipitation_value" => precipitation ? precipitation[0] : "-",
                        "precipitation_probability" => precipitation ? precipitation[1] * h[period]: "-"}
    return precipitation_json
  end

  def predict_temp_data(stations,time,period)
    temps = []
    stations.each do |station|
      times = []
      temp = []
      @valid = station.readerbs.select {|p| p.time.hour == time.hour}
      @valid.each do |p|
        if (p.temperature && p.temperature.temp)
          times << time_transfer(p.time)
          temp << p.temperature.temp
        end
      end
      @valid = station.readers.select {|p| p.time.hour == time.hour}
      @valid.each do |p|
        if (p.temperature && p.temperature.temp)
          times << time_transfer(p.time)
          temp << p.temperature.temp
        end
      end
      temps << [times,temp]
    end
    @prediction = Prediction.new()
    h = {0 => 1, 10 => 0.9, 30 => 0.8, 60 => 0.7, 120 => 0.6, 180 => 0.5}
    temperature = @prediction.get_regress(time_transfer(time)+period,temps,40)
    temp_json = { "temperature_value" => temperature ? temperature[0] : "-",
                  "temperature_probability" => temperature ? temperature[1] * h[period] : "-"}
    print "temp"
    print temps
    return temp_json
  end

  def predict_wind_data(stations,time,period)
    wind_speeds = []
    wind_bearings = []
    stations.each do |station|
      times = []
      wind_speed = []
      wind_bearing = []
      @valid = station.readerbs.select {|p| p.time.to_date == time.to_date}
      @valid.each do |p|
        if (p.wind && p.wind.speed && p.wind.bearing)
        #if (p.wind && p.wind.speed && p.wind.direction)
          times << time_transfer(p.time)
          wind_speed << p.wind.speed
          wind_bearing << p.wind.bearing
          #wind_bearing << direction_to_bearing(p.wind.direction)
        end
      end
      @valid = station.readers.select {|p| p.time.to_date == time.to_date}
      @valid.each do |p|
        #if (p.wind && p.wind.speed && p.wind.bearing)
        if (p.wind && p.wind.speed && p.wind.direction)
          times << time_transfer(p.time)
          wind_speed << p.wind.speed
          #wind_bearing << p.wind.bearing
          wind_bearing << direction_to_bearing(p.wind.direction)
        end
      end
      wind_speeds << [times, wind_speed]
      wind_bearings << [times, wind_bearing]
    end
    @prediction = Prediction.new()
    h = {0 => 1, 10 => 0.9, 30 => 0.8, 60 => 0.7, 120 => 0.6, 180 => 0.5}
    wind_speed = @prediction.get_regress(time_transfer(time)+period,wind_speeds,100)
    wind_bearing = @prediction.get_regress(time_transfer(time)+period,wind_bearings,360)
    wind_speed_json = { "wind_speed_value" => wind_speed ? wind_speed[0] : "-",
                            "wind_speed_probability" => wind_speed ? wind_speed[1] * h[period]: "-"}
    wind_direction_json = { "wind_direction_value" => (wind_speed && wind_bearing) ? bearing_to_direction(wind_bearing[0],wind_speed[0]) : "-",
                            "wind_direction_probability" => (wind_speed && wind_bearing) ? wind_bearing[1] * h[period]: "-"}
    return [wind_speed_json, wind_direction_json]
  end

  def predict_by_time(s,time,period)
    timen = time + period * 60
    precip = predict_precip_data(s,time,period)
    temp = predict_temp_data(s,time,period)
    wind = predict_wind_data(s,time,period)
    predict_json = {"time" => timen.strftime("%I:%M%P %d-%m-%Y"),
                    "precip" => precip,
                    "temp" => temp,
                    "wind_speed" => wind[0],
                    "wind_direction" => wind[1]}
    return predict_json
  end

  def bearing_to_direction(bearing, speed)
    if speed == 0
      return "CALM"
    end
    dirs = ["N","NNE","NE","ENE","E","ESE","SE","SSE","S","SSW","SW","WSW","W","WNW","NW","NNW"];
    n = (bearing/22.5).round(0) % 16
    return dirs[n]
  end

  def direction_to_bearing(direction)
    h = {"N" => 0, "NNE" => 22.5, "NE" => 45, "ENE" => 67.5, "E" => 90, "ESE" => 112.5, "SE" => 135, "SSE" => 157.5,
    "S" => 180, "SSW" => 202.5, "SW" => 225, "WSW" => 247.5, "W" => 270, "WNW" => 292.5, "NW" => 315, "NNW" => 337.5,
    "CALM" => 0}
    return h[direction]
  end

end
