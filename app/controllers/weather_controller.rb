class WeatherController < ApplicationController
  #before_action :find_location, only: [:find_by_loc_date]
  #before_action :find_postcode_location, only: [:find_by_pcode_date]
  before_action :prepare_location, only: [:find_weather_data, :search_weather_data]
  POST_REG = /\d/
  ID_REG = /[[:alpha:]]/

  def data
  	@stations = Station.all
  end

  def locations
  	@locations = Station.all

  	respond_to do |format|
  		format.html
  		format.json {render json: index_locations}
  	end
  end


  def find_weather_data
    respond_to do |format|
      if (POST_REG.match(params[:id_or_pcode]))

        format.html {render template: "weather/postcode_data"}
        format.json {render json: data_by_postcode}

      elsif (ID_REG.match(params[:id_or_pcode]))
        format.html {render 'weather/location_data'}
        format.json {render json: data_by_id}
      end
    end
  end


  private

  def index_locations
    today = Time.new.strftime("%d-%m-%Y")
    locations = []
    @locations.each do |l|
      locations << l.to_json
    end
    locations_all = {"date" => today, "locations" => locations}
  end

  def data_by_postcode
    begin
      date_string = params[:date]
      d = Date.parse(date_string)
      locations_array = []

      @locations.each do |l|
        temp = l.to_json
        temp[:measurement] = all_measurement(l, d)
        locations_array << temp
        
      end
      pcode_data_json = {"date" => d, "location" => locations_array}
      pcode_data_json
    rescue => e
      {:error => "Could not process measurement: #{e}"}
    end
  end

  def data_by_id
    begin
      date_string = params[:date]
      d = Date.parse(date_string)
      # build json response.

      locID_data_json = {"date" => d, 
        "current_temp" => @location.current_temp,
        "current_cond" => @location.current_cond,
        "measurement" => all_measurement(@location, d)}
      locID_data_json
    rescue => e
      {:error => "Could not process measurement: #{e}"}.to_json
    end
  end

  def all_measurement location, date
    measurements = []

    valid = location.readers.select {|m| m.time.to_date == date}
    valid.each do |v|
      measurements << v.to_json
    end
    return measurements
  end

  def prepare_location
    if (POST_REG.match(params[:id_or_pcode]))
      find_location_by_postcode
    elsif (ID_REG.match(params[:id_or_pcode]))
      find_location_by_id
    else
      render json: {:error => "INPUT provided is NOT valid."}.to_json
    end
  end

  def find_location_by_id
    if (Station.exists?(name: params[:id_or_pcode]))
      @location = Station.find_by(name: params[:id_or_pcode])
    else
      render json: {:error => "location_id provided is NOT valid."}.to_json
    end
  end

  def find_location_by_postcode
    if (Station.exists?(postcode: params[:id_or_pcode]))
      @locations = Station.select{|p| p.postcode == params[:id_or_pcode].to_i}
    else
      render json: {:error => "postcode provided is NOT valid."}.to_json
    end
  end

end
