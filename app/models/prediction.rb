class Prediction
	
	def get_regress(timen,data,threshold)
		n = 0
		value = 0
		prob = 0
		(0...data.length).each do |i|#each station
			if data[i][1].length > 1#if the station has more than 1 data
				regress = Regression.new(data[i][0], data[i][1])
				y = regress.calculate(timen,threshold)
				if y[1] >= 0.2
					value += y[0]
					prob += y[1]
					#print y[0], " "
					n += 1
				end
			end
		end
		if n > 0
			value /= n
			prob /= n
			#print ": ", value, "\n"
			return [value.round(2), prob.round(2)]
		else
			return nil
		end
	end

	
end





