class Reader < ActiveRecord::Base
	belongs_to :day
	belongs_to :station
	belongs_to :wind
	belongs_to :temperature
	belongs_to :dew
	belongs_to :rain

	def to_json
		begin
			weather_details = {"time" => time.strftime("%I:%M:%S %P"), 
				"temp" => temperature ? temperature.temp : "-", 
				"precip" => rain ? rain.amount : "-", 
				"wind_direction" => wind ? wind.direction : "-", 
				"wind_speed" => wind ? wind.speed : "-"}
			weather_details
		rescue => e
			"no measurement"
		end
	end

end
