class Readerb < ActiveRecord::Base
	belongs_to :station
	belongs_to :wind
	belongs_to :temperature
	belongs_to :dew
	belongs_to :rain

	def to_json
		weather_details = {"datetime" => time, "temp" => temperature.temp, 
			"wind_speed" => wind.speed, "precip" => rain.amount}
		weather_details
	end

end
