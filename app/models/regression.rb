require "matrix.rb"
class Regression

     attr_accessor :x_array, :y_array

     def initialize(dataX, dataY)
          @x_array = dataX
          @y_array = dataY
          @rsquares = Array.new(4)
          @prob = Array.new(4)
          @y_ave = 0
          @n = @y_array.length

          print "===TESTING==== \n"
          (0...@n).each do |i|
               print @y_array[i] 
               print "\n"
          end

          (0...@n).each do |i|
               @y_ave += @y_array[i]
          end
          @y_ave=@y_ave/@n
          @bestFitCoeff = Array.new(4)
          @bestFitType = Array.new(4)
     end

     def rsquare y1_array
          sse = 0
          ssr = 0
          (0...@n).each do |i|
               sse += (@y_array[i] - y1_array[i]) ** 2
               ssr += (y1_array[i] - @y_ave) ** 2
          end
          sst = sse + ssr
          if sst == 0
               rs = 1
          else
               rs = ssr / sst
          end
          return rs
     end



     def linear_regress #x_array, y_array
          begin    
               n = Float(@x_array.length)
               x_ave = (eval @x_array.join('+')) / n
               y_ave = (eval @y_array.join('+')) / n
               mx = Matrix[@x_array]
               my = Matrix.column_vector(@y_array)
               b1 = ((mx * my).to_a[0][0] - n * x_ave * y_ave) / ((mx * mx.t).to_a[0][0] - n * x_ave ** 2)
               b0 = y_ave - b1 * x_ave
               b1 = (b1 * 100).round.to_f/100
               b0 = (b0 * 100).round.to_f/100
               y1_array = @x_array.map { |item| b1 * item + b0 }
               my1 = Matrix.column_vector(y1_array)

               delta = rsquare(y1_array)
               #puts "linear: #{delta}"

               #delta = ((my.t - my1.t) * (my - my1)).to_a[0][0] / n
               #puts "linear: #{delta}"
               result = Array([delta, b1, b0])
          rescue
               result = Array([-1])
          end
          return result
     end

     def polynomial_regress degree#x_array, y_array, degree
          begin 
               n = Float(@x_array.length)
               x_data = @x_array.map { |x_i| (0..degree).map { |pow| (x_i**pow).to_f } }
               mx = Matrix[*x_data]
               my = Matrix.column_vector(@y_array)
               coefficients = (mx.t * mx).inv * mx.t * my
               coeff = coefficients.map{ |item| (item * 100).round.to_f/100 }
               my1 = mx * coeff

               delta = rsquare(my1.column(0))
               #puts "poly #{degree}: #{delta}"

               #delta = ((my.t - my1.t) * (my - my1)).to_a[0][0] / n
               #puts "poly #{degree}: #{delta}"
               result = Array(delta) + coeff.transpose.to_a[0].reverse
          rescue
               result = Array([-1])
          end
          return result
     end

     def exponential_regress #x_array, y_array
          begin
               n = Float(@x_array.length)
               yln_array = @y_array.map { |item| Math.log(item)} #log
               yln_ave = (eval yln_array.join('+')) / n
               x_ave = (eval @x_array.join('+')) / n
               mx = Matrix[@x_array]
               my = Matrix.column_vector(@y_array)
               myln = Matrix.column_vector(yln_array)
               a = (yln_ave * (mx * mx.t).to_a[0][0] - x_ave * (mx * myln).to_a[0][0]) / ((mx * mx.t).to_a[0][0] - n * x_ave ** 2)
               b = ((mx * myln).to_a[0][0] - n * x_ave * yln_ave) / ((mx * mx.t).to_a[0][0] - n * x_ave ** 2)
               a1 = (Math.exp(a) * 100).round.to_f/100
               b = (b * 100).round.to_f/100
               y1_array = @x_array.map { |item| a1 * Math.exp(b * item) }
               my1 = Matrix.column_vector(y1_array)

               delta = rsquare(y1_array)
               #puts "exp: #{delta}"

               #delta = ((my.t - my1.t) * (my - my1)).to_a[0][0] / n
               #puts "exp: #{delta}"
               result = Array([delta, a1, b])
          rescue
               result = Array([-1])
          end
          return result
     end

     def logarithmic_regress #x_array, y_array
          begin
              n = Float(@x_array.length)
              xln_array = @x_array.map { |item| Math.log(item)} #log
               xln_ave = (eval xln_array.join('+')) / n
               y_ave = (eval @y_array.join('+')) / n
               mx = Matrix[xln_array]
               my = Matrix.column_vector(@y_array)
               b = ((mx * my).to_a[0][0] - n * xln_ave * y_ave) / ((mx * mx.t).to_a[0][0] - n * xln_ave ** 2)
               a = y_ave - b * xln_ave
               b = (b * 100).round.to_f/100
               a = (a * 100).round.to_f/100
               y1_array = @x_array.map { |item| b * Math.log(item) + a }
               my1 = Matrix.column_vector(y1_array)

               delta = rsquare(y1_array)
               #puts "log: #{delta}"

               #delta = ((my.t - my1.t) * (my - my1)).to_a[0][0] / n
               #puts "log: #{delta}"
               result = Array([delta, b, a])
          rescue
               result = Array([-1])
          end
          return result   
     end

     def best_fit
          @rsquares = [0, 0, 0, 0]

          @rsquares[0] = linear_regress()[0] 
          @rsquares[1] = exponential_regress()[0]
          @rsquares[2] = logarithmic_regress()[0]

          polynomial_results = Array.new(9)
          (0..8).each {|index| polynomial_results[index] = polynomial_regress(index+2)[0]}
          @rsquares[3] = polynomial_results.each_with_index.max[0]

          (0..3).each do |i|
               @prob[i] = @rsquares.each_with_index.max[0]
               if @prob[i] == -1
                    @bestFitCoeff[i] = nil
               else

                    case @rsquares.each_with_index.max[1]
                    when 0
               #linear_print(linear_regress)
               @bestFitCoeff[i] = linear_regress[1..2]#(time,datapoint)
               @bestFitType[i] = :linear
               @rsquares[0] = -2
                    when 1
               #exponential_print(exponential_regress)
               @bestFitCoeff[i] = exponential_regress[1..2]#(time,datapoint)
               @bestFitType[i] = :exponential
               @rsquares[1] = -2
                    when 2
               #logarithmic_print(logarithmic_regress)
               @bestFitCoeff[i] = logarithmic_regress[1..2]#(time,datapoint)
               @bestFitType[i] = :logarithmic
               @rsquares[2] = -2
                    when 3
               best_degree = polynomial_results.each_with_index.max[1] + 2
               #polynomial_print(polynomial_regress(best_degree))
               @bestFitCoeff[i] = polynomial_regress(best_degree)[1..(best_degree+1)]
               @bestFitType[i] = :polynomial
               @rsquares[3] = -2
                    end

               end
          end
     end

     def calculate(newX,threshold)
          (0...@n).each do |i|
               if newX - @x_array[i] <= 5
                    return [@y_array[i], 1.0]
               end
          end

          result = Array.new(2)
          best_fit #
          (0..3).each do |i|
               if @bestFitCoeff[i]

                    case @bestFitType[i]
                    when :linear
                         newY = @bestFitCoeff[i][0] * newX + @bestFitCoeff[i][1]
                    when :exponential
                         newY = @bestFitCoeff[i][0] * Math.exp(@bestFitCoeff[i][1] * newX)
                    when :logarithmic
                         newY = @bestFitCoeff[i][0] * Math.log(newX) + @bestFitCoeff[i][1]
                    when :polynomial
                         newY = 0
                         degree = @bestFitCoeff[i].length - 1
                         (0..degree).each do |j|
                              newY += @bestFitCoeff[i][j] * (newX ** (degree - j))
                         end
                    end

                    if newY >= 0 && newY <= threshold
                         result[0] = newY
                         result[1] = @prob[i]
                         return result
                    end

               end
          end
          result[0] = 0
          result[1] = 0
          #puts "For #{newX}, the result of regression #{@bestFitType} is #{result[0]}, probablity is #{result[1]}"
          return result
     end


end

