class Station < ActiveRecord::Base
	has_many :readers
	#has_many :winds, :through => :readers
	has_many :readerbs

	has_many :days

	def to_json

		station_details = {'id' => name,
							'lat' => latitude, 'lon' => longitude, 
							'last_update' => readerbs.last.updated_at.strftime("%I:%M%P %d-%m-%Y")}
		station_details
	end

	def current_temp
		begin
			last_time = readers.last.temperature.created_at
			if ((Time.now() - last_time).round.abs <= 30*60)
				return readers.last.temperature.temp
			else
				return "-"
			end
		rescue => e
			return "-"
		end
	end

	def current_cond
		begin
			last_time = readerbs.last.temperature.created_at
			if ((Time.now() - last_time).round.abs <= 30*60)
				return readerbs.last.temperature.icon
			else
				return "-"
			end
		rescue => e
			return "-"
		end
	end

	def get_distance(querylat,querylon)
		d = ((latitude - querylat) ** 2 + (longitude - querylon) ** 2) ** (1.0/2)
		#puts d
		return d
	end

end
