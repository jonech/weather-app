
require 'rake'

Project2r::Application.load_tasks

class Scraper
	def perform
		Rake::Task['scrape:all'].invoke
	end
end

Crono.perform(Scraper).every 10.minutes