# Load the Rails application.
require File.expand_path('../application', __FILE__)
require 'rake'  
load File.join(Rails.root, 'lib', 'tasks', 'scraper.rake')

# Initialize the Rails application.
Rails.application.initialize!
