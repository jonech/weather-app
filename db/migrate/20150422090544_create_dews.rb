class CreateDews < ActiveRecord::Migration
  def change
    create_table :dews do |t|
      t.decimal :dew_point

      t.timestamps null: false
    end
  end
end
