class CreateRains < ActiveRecord::Migration
  def change
    create_table :rains do |t|
      t.decimal :amount

      t.timestamps null: false
    end
  end
end
