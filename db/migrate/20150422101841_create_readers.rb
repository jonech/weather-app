class CreateReaders < ActiveRecord::Migration
  def change
    create_table :readers do |t|
      t.datetime :time
      t.string :url

      t.timestamps null: false
    end
  end
end
