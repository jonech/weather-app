class AddWindToReader < ActiveRecord::Migration
  def change
    add_reference :readers, :wind, index: true, foreign_key: true
  end
end
