class AddReaderToStation < ActiveRecord::Migration
  def change
    add_reference :stations, :reader, index: true, foreign_key: true
  end
end
