class AddStationToReader < ActiveRecord::Migration
  def change
    add_reference :readers, :station, index: true, foreign_key: true
  end
end
