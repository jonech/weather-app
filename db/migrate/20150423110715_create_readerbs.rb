class CreateReaderbs < ActiveRecord::Migration
  def change
    create_table :readerbs do |t|
      t.datetime :time

      t.timestamps null: false
    end
  end
end
