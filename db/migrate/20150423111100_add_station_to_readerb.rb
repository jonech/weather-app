class AddStationToReaderb < ActiveRecord::Migration
  def change
    add_reference :readerbs, :station, index: true, foreign_key: true
  end
end
