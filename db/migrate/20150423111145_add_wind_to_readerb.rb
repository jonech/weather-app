class AddWindToReaderb < ActiveRecord::Migration
  def change
    add_reference :readerbs, :wind, index: true, foreign_key: true
  end
end
