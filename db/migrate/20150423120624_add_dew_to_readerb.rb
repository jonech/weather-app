class AddDewToReaderb < ActiveRecord::Migration
  def change
    add_reference :readerbs, :dew, index: true, foreign_key: true
  end
end
