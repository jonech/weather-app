class AddTemperatureToReaderb < ActiveRecord::Migration
  def change
    add_reference :readerbs, :temperature, index: true, foreign_key: true
  end
end
