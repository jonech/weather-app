class AddRainToReaderb < ActiveRecord::Migration
  def change
    add_reference :readerbs, :rain, index: true, foreign_key: true
  end
end
