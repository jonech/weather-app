class AddRefToReader < ActiveRecord::Migration
  def change
    add_reference :readers, :dew, index: true, foreign_key: true
    add_reference :readers, :temperature, index: true, foreign_key: true
    add_reference :readers, :rain, index: true, foreign_key: true
  end
end
