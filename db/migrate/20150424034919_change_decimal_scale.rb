class ChangeDecimalScale < ActiveRecord::Migration
  def change
  	change_column :temperatures, :current_temperature, :decimal, precision: 30, scale: 2
  	change_column :winds, :speed, :decimal, precision: 30, scale:2
  	change_column :rains, :amount, :decimal, precision: 30, scale: 2
  	change_column :dews, :dew_point, :decimal, precision: 30, scale: 2
  end
end
