class AddStationToDay < ActiveRecord::Migration
  def change
    add_reference :days, :station, index: true, foreign_key: true
  end
end
