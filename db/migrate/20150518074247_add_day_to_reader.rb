class AddDayToReader < ActiveRecord::Migration
  def change
    add_reference :readers, :day, index: true, foreign_key: true
  end
end
