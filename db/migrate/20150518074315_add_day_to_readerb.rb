class AddDayToReaderb < ActiveRecord::Migration
  def change
    add_reference :readerbs, :day, index: true, foreign_key: true
  end
end
