class AddIconToTemperature < ActiveRecord::Migration
  def change
  	add_column :temperatures, :icon, :string
  	rename_column :temperatures, :current_temperature, :temp
  end
end
