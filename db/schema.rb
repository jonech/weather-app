# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150527053402) do

  create_table "crono_jobs", force: :cascade do |t|
    t.string   "job_id",            null: false
    t.text     "log"
    t.datetime "last_performed_at"
    t.boolean  "healthy"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "crono_jobs", ["job_id"], name: "index_crono_jobs_on_job_id", unique: true

  create_table "days", force: :cascade do |t|
    t.date     "date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "station_id"
  end

  add_index "days", ["station_id"], name: "index_days_on_station_id"

  create_table "dews", force: :cascade do |t|
    t.decimal  "dew_point",  precision: 30, scale: 2
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "rains", force: :cascade do |t|
    t.decimal  "amount",     precision: 30, scale: 2
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "readerbs", force: :cascade do |t|
    t.datetime "time"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "station_id"
    t.integer  "wind_id"
    t.integer  "dew_id"
    t.integer  "temperature_id"
    t.integer  "rain_id"
    t.integer  "day_id"
  end

  add_index "readerbs", ["day_id"], name: "index_readerbs_on_day_id"
  add_index "readerbs", ["dew_id"], name: "index_readerbs_on_dew_id"
  add_index "readerbs", ["rain_id"], name: "index_readerbs_on_rain_id"
  add_index "readerbs", ["station_id"], name: "index_readerbs_on_station_id"
  add_index "readerbs", ["temperature_id"], name: "index_readerbs_on_temperature_id"
  add_index "readerbs", ["wind_id"], name: "index_readerbs_on_wind_id"

  create_table "readers", force: :cascade do |t|
    t.datetime "time"
    t.string   "url"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "wind_id"
    t.integer  "station_id"
    t.integer  "dew_id"
    t.integer  "temperature_id"
    t.integer  "rain_id"
    t.integer  "day_id"
  end

  add_index "readers", ["day_id"], name: "index_readers_on_day_id"
  add_index "readers", ["dew_id"], name: "index_readers_on_dew_id"
  add_index "readers", ["rain_id"], name: "index_readers_on_rain_id"
  add_index "readers", ["station_id"], name: "index_readers_on_station_id"
  add_index "readers", ["temperature_id"], name: "index_readers_on_temperature_id"
  add_index "readers", ["wind_id"], name: "index_readers_on_wind_id"

  create_table "stations", force: :cascade do |t|
    t.string   "name"
    t.decimal  "latitude"
    t.decimal  "longitude"
    t.decimal  "height"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "reader_id"
    t.integer  "postcode"
  end

  add_index "stations", ["reader_id"], name: "index_stations_on_reader_id"

  create_table "temperatures", force: :cascade do |t|
    t.decimal  "temp",       precision: 30, scale: 2
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "icon"
  end

  create_table "winds", force: :cascade do |t|
    t.decimal  "speed",      precision: 30, scale: 2
    t.string   "direction"
    t.integer  "bearing"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

end
