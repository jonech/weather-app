# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Station.delete_all

Station.create(name: 'Charlton', latitude: '-36.28', longitude: '143.33', height: '131.7', postcode: '3525')
Station.create(name: 'Hopetoun Airport', latitude: '-35.72', longitude: '142.36', height: '77.3', postcode: '3396')
Station.create(name: 'Kerang', latitude: '-35.72', longitude: '143.92', height: '77.7', postcode: '3579')
Station.create(name: 'Mildura', latitude: '-34.24', longitude: '142.09', height: '50.0', postcode: '3500')
Station.create(name: 'Ouyen', latitude: '-35.07', longitude: '142.31', height: '65.0', postcode: '3490')
Station.create(name: 'Swan Hill', latitude: '-35.38', longitude: '143.54', height: '71.0', postcode: '3585')
Station.create(name: 'Walpeup RS', latitude: '-35.12', longitude: '142.00', height: '105.0', postcode: '3507')
Station.create(name: 'Edenhope', latitude: '-37.02', longitude: '141.27', height: '155.0', postcode: '3318')
Station.create(name: 'Horsham', latitude: '-36.67', longitude: '142.17', height: '133.8', postcode: '3400')
Station.create(name: 'Kanagulk', latitude: '-37.12', longitude: '141.80', height: '188.8', postcode: '3401')
Station.create(name: 'Longerenong', latitude: '-36.67', longitude: '142.30', height: '133.0', postcode: '3401')
=begin
Station.create(name: 'Nhill Aerodrome', latitude: '-36.31', longitude: '141.65', height: '138.9', postcode: '3418')
Station.create(name: 'Stawell', latitude: '-37.07', longitude: '142.74', height: '235.36', postcode: '3380')
Station.create(name: 'Warracknabeal', latitude: '-36.26', longitude: '142.41', height: '113.4', postcode: '3393')
Station.create(name: 'Ararat', latitude: '-37.28', longitude: '142.98', height: '295.0', postcode: '3377')
Station.create(name: 'Ben Nevis', latitude: '-37.23', longitude: '143.20', height: '875.0', postcode: '3377')
Station.create(name: 'Cape Nelson', latitude: '-38.43', longitude: '141.54', height: '45.4', postcode: '3305')
Station.create(name: 'Cape Otway', latitude: '-38.86', longitude: '143.51', height: '82.0', postcode: '3233')
Station.create(name: 'Casterton', latitude: '-37.58', longitude: '141.33', height: '130.6', postcode: '3311')
Station.create(name: 'Dartmoor', latitude: '-37.92', longitude: '141.26', height: '51.0', postcode: '3304')
Station.create(name: 'Hamilton', latitude: '-37.65', longitude: '142.06', height: '241.1', postcode: '3300')
Station.create(name: 'Mortlake', latitude: '-38.07', longitude: '142.77', height: '130.0', postcode: '3272')
Station.create(name: 'Mount Gellibrand', latitude: '-38.23', longitude: '143.79', height: '261.0', postcode: '3241')
Station.create(name: 'Mount William', latitude: '-37.30', longitude: '142.60', height: '1150.0', postcode: '')
Station.create(name: 'Port Fairy', latitude: '-38.39', longitude: '142.23', height: '10.0', postcode: '3284')
Station.create(name: 'Portland Airport', latitude: '-38.31', longitude: '141.47', height: '80.9', postcode: '3305')
Station.create(name: 'Portland Harbour', latitude: '-38.34', longitude: '141.61', height: '0.0', postcode: '3305')
Station.create(name: 'Warrnambool', latitude: '-38.29', longitude: '142.45', height: '70.8', postcode: '3275')
Station.create(name: 'Westmere', latitude: '-37.71', longitude: '142.94', height: '226.0', postcode: '3351')
Station.create(name: 'Aireys Inlet', latitude: '-38.46', longitude: '144.09', height: '95.0', postcode: '3231')
Station.create(name: 'Avalon', latitude: '-38.03', longitude: '144.48', height: '10.6', postcode: '3212')
Station.create(name: 'Ballarat', latitude: '-37.51', longitude: '143.79', height: '435.2', postcode: '3352')
Station.create(name: 'Bundoora', latitude: '-37.72', longitude: '145.05', height: '83.0', postcode: '3073')
Station.create(name: 'Cerberus', latitude: '-38.36', longitude: '145.18', height: '12.69', postcode: '3918')
Station.create(name: 'Coldstream', latitude: '-37.72', longitude: '145.41', height: '83.0', postcode: '3770')
Station.create(name: 'Cranbourne', latitude: '-38.13', longitude: '145.26', height: '85.0', postcode: '3977')
Station.create(name: 'Essendon Airport', latitude: '-37.73', longitude: '144.91', height: '78.4', postcode: '3040')
Station.create(name: 'Fawkner Beacon', latitude: '-37.91', longitude: '144.93', height: '17.0', postcode: '3060')
Station.create(name: 'Ferny Creek', latitude: '-37.87', longitude: '145.35', height: '512.9', postcode: '3154')
Station.create(name: 'Frankston', latitude: '-38.15', longitude: '145.12', height: '6.0', postcode: '3199')
Station.create(name: 'Geelong Racecourse', latitude: '-38.17', longitude: '144.38', height: '12.9', postcode: '3219')
Station.create(name: 'Laverton', latitude: '-37.86', longitude: '144.76', height: '20.1', postcode: '3028')
Station.create(name: 'Melbourne Airport', latitude: '-37.67', longitude: '144.83', height: '113.4', postcode: '3036')
Station.create(name: 'Melbourne (Olympic Park)', latitude: '-37.83', longitude: '144.98', height: '7.53', postcode: '3000')
Station.create(name: 'Moorabbin Airport', latitude: '-37.98', longitude: '145.10', height: '12.1', postcode: '3172')
Station.create(name: 'Phillip Island', latitude: '-38.51', longitude: '145.15', height: '7.0', postcode: '')
Station.create(name: 'Point Wilson', latitude: '-38.10', longitude: '144.54', height: '18.0', postcode: '3212')
Station.create(name: 'Pound Creek', latitude: '-38.63', longitude: '145.81', height: '3.0', postcode: '3996')
Station.create(name: 'Rhyll', latitude: '-38.46', longitude: '145.31', height: '13.4', postcode: '3923')
Station.create(name: 'Scoresby', latitude: '-37.87', longitude: '145.26', height: '80.0', postcode: '3152')
Station.create(name: 'Sheoaks', latitude: '-37.91', longitude: '144.13', height: '236.7', postcode: '3331')
Station.create(name: 'South Channel Island', latitude: '-38.31', longitude: '144.80', height: '9.0', postcode: '3225')
Station.create(name: 'St Kilda Harbour RMYS', latitude: '-37.86', longitude: '144.96', height: '6.4', postcode: '3004')
Station.create(name: 'Viewbank', latitude: '-37.74', longitude: '145.10', height: '66.1', postcode: '3084')
Station.create(name: 'Wonthaggi', latitude: '-38.61', longitude: '145.60', height: '51.9', postcode: '3995')
Station.create(name: 'Bendigo', latitude: '-36.74', longitude: '144.33', height: '208.0', postcode: '3550')
Station.create(name: 'Echuca', latitude: '-36.16', longitude: '144.76', height: '96.0', postcode: '3564')
Station.create(name: 'Kyabram', latitude: '-36.34', longitude: '145.06', height: '105.0', postcode: '3619')
Station.create(name: 'Mangalore', latitude: '-36.89', longitude: '145.19', height: '140.8', postcode: '3663')
Station.create(name: 'Redesdale', latitude: '-37.02', longitude: '144.52', height: '290.0', postcode: '3444')
Station.create(name: 'Shepparton', latitude: '-36.43', longitude: '145.39', height: '113.9', postcode: '3631')
Station.create(name: 'Strathbogie', latitude: '-36.85', longitude: '145.73', height: '502.0', postcode: '3666')
Station.create(name: 'Tatura', latitude: '-36.44', longitude: '145.27', height: '114.0', postcode: '3616')
Station.create(name: 'Yarrawonga', latitude: '-36.03', longitude: '146.03', height: '128.9', postcode: '3730')
Station.create(name: 'Albury', latitude: '-36.07', longitude: '146.95', height: '163.5', postcode: '3690')
Station.create(name: 'Benalla', latitude: '-36.55', longitude: '146.00', height: '170.5', postcode: '3671')
Station.create(name: 'Corryong Airport', latitude: '-36.18', longitude: '147.89', height: '290.0', postcode: '3707')
Station.create(name: 'Falls Creek', latitude: '-36.87', longitude: '147.28', height: '1765.0', postcode: '3699')
Station.create(name: 'Hunters Hill', latitude: '-36.21', longitude: '147.54', height: '981.0', postcode: '3701')
Station.create(name: 'Lake Dartmouth', latitude: '-36.54', longitude: '147.50', height: '365.0', postcode: '3701')
Station.create(name: 'Mount Buller', latitude: '-37.15', longitude: '146.44', height: '1707.0', postcode: '3723')
Station.create(name: 'Mount Hotham Airport', latitude: '-37.05', longitude: '147.33', height: '1295.4', postcode: '3741')
Station.create(name: 'Mount Hotham AWS', latitude: '-36.98', longitude: '147.13', height: '1849.0', postcode: '3741')
Station.create(name: 'Rutherglen RS', latitude: '-36.10', longitude: '146.51', height: '175.0', postcode: '3685')
Station.create(name: 'Wangaratta', latitude: '-36.42', longitude: '146.31', height: '152.6', postcode: '3677')
Station.create(name: 'Castlemaine', latitude: '-37.08', longitude: '144.24', height: '330.0', postcode: '3450')
Station.create(name: 'Eildon Fire Tower', latitude: '-37.21', longitude: '145.84', height: '637.0', postcode: '3041')
Station.create(name: 'Kilmore Gap', latitude: '-37.38', longitude: '144.97', height: '527.8', postcode: '3762')
Station.create(name: 'Lake Eildon', latitude: '-37.23', longitude: '145.91', height: '230.0', postcode: '3713')
Station.create(name: 'Maryborough', latitude: '-37.06', longitude: '143.73', height: '249.3', postcode: '3465')
Station.create(name: 'East Sale', latitude: '-38.12', longitude: '147.13', height: '4.6', postcode: '3852')
Station.create(name: 'Hogan Island', latitude: '-39.22', longitude: '146.98', height: '116.0', postcode: '7255')
Station.create(name: 'Latrobe Valley', latitude: '-38.21', longitude: '146.47', height: '55.7', postcode: '3844')
Station.create(name: 'Mount Baw Baw', latitude: '-37.84', longitude: '146.27', height: '1561.0', postcode: '3833')
Station.create(name: 'Mount Moornapa', latitude: '-37.75', longitude: '147.14', height: '480.0', postcode: '3862')
Station.create(name: 'Warragul (Nilma North)', latitude: '-38.13', longitude: '145.99', height: '134.11', postcode: '3821')
Station.create(name: 'Willow Grove', latitude: '-38.08', longitude: '146.21', height: '161.0', postcode: '3825')
Station.create(name: 'Wilsons Promontory', latitude: '-39.13', longitude: '146.42', height: '95.0', postcode: '3960')
Station.create(name: 'Yanakie', latitude: '-38.81', longitude: '146.19', height: '13.3', postcode: '3960')
Station.create(name: 'Yarram Airport', latitude: '-38.56', longitude: '146.75', height: '17.9', postcode: '')
Station.create(name: 'Bairnsdale', latitude: '-37.88', longitude: '147.57', height: '49.4', postcode: '3875')
Station.create(name: 'Combienbar', latitude: '-37.34', longitude: '149.02', height: '640.0', postcode: '3889')
Station.create(name: 'Gabo Island', latitude: '-37.57', longitude: '149.92', height: '15.2', postcode: '')
Station.create(name: 'Gelantipy', latitude: '-37.22', longitude: '148.26', height: '755.0', postcode: '3885')
Station.create(name: 'Kingfish B', latitude: '-38.60', longitude: '148.19', height: '31.4', postcode: '4655')
Station.create(name: 'Lakes Entrance', latitude: '-37.87', longitude: '148.01', height: '4.0', postcode: '3909')
Station.create(name: 'Mallacoota', latitude: '-37.60', longitude: '149.73', height: '22.0', postcode: '3892')
Station.create(name: 'Mount Nowa Nowa', latitude: '-37.69', longitude: '148.09', height: '350.0', postcode: '3887')
Station.create(name: 'Omeo', latitude: '-37.10', longitude: '147.60', height: '689.8', postcode: '3898')
Station.create(name: 'Orbost', latitude: '-37.69', longitude: '148.47', height: '62.65', postcode: '3888')
Station.create(name: 'Point Hicks', latitude: '-37.80', longitude: '149.27', height: '27.0', postcode: '3891')
Station.create(name: 'Victoria Point', latitude: '-37.45', longitude: '142.19', height: '136.0', postcode: '3294')
Station.create(name: 'Barwon Downs', latitude: '-38.46', longitude: '143.76', height: '144.0', postcode: '3243') 
=end
