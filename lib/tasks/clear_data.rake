desc "clear all records in Station, Temperature, Rain, Dew, Wind, and Readers"
task :delete_data => :environment do
	Temperature.delete_all
	Rain.delete_all
	Dew.delete_all
	Wind.delete_all
	Reader.delete_all
	Readerb.delete_all
	Station.delete_all
end