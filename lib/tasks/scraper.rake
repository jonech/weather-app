require 'nokogiri'
require 'open-uri'
require 'json'

namespace :scrape do
	desc "scrape data from http://www.bom.gov.au/vic/observations/melbourne.shtml"
	task :bom => :environment do

		DIG_REG = /\d/
		APLH_REG = /[[:alpha:]]/
		t = Time.now()

		
		url = 'http://www.bom.gov.au/vic/observations/vicall.shtml'
		doc = Nokogiri::HTML(open(url))

	    @stations = Station.all

		doc.css("tr.rowleftcolumn").each do |data|

			@stations.each do |station|

				area = data.at_css("th.rowleftcolumn").text
				rain_val = data.at_css("td:nth-child(14)").text
				temp = data.at_css("td:nth-child(3)").text
				dew_val = data.at_css("td:nth-child(5)").text
				wind_spd = data.at_css("td:nth-child(9)").text
				wind_dir = data.at_css("td:nth-child(8)").text

				if station.name == area
					@reading = station.readers.create(time: t)

					@w = Wind.create(speed: wind_spd, direction: wind_dir)
					@reading.wind_id = @w.id
					if DIG_REG.match(rain_val)
						@r = Rain.create(amount: rain_val)
						@reading.rain_id = @r.id
					end

					if DIG_REG.match(dew_val)
						@d = Dew.create(dew_point: dew_val)
						@reading.dew_id = @d.id
					end

					if DIG_REG.match(temp)
						@t = Temperature.create(temp: temp)
						@reading.temperature_id = @t.id
					end
					
					@reading.save
				end
			end
			
		end
	end

	desc "scrape data of forecast.io"
	task :forecast => :environment do

		# Define the URL
		api_key = '58fca16add9ba66cbb0a07e88a43a8d2'
		base_url = 'https://api.forecast.io/forecast'

		@stations = Station.all
		t = Time.now()
		
		@stations.each do |station|
			lat = station.latitude.to_s
			long = station.longitude.to_s
			lat_long= lat + ',' + long

			full_url = "#{base_url}/#{api_key}/#{lat_long}"
			output = open(full_url, {ssl_verify_mode: OpenSSL::SSL::VERIFY_NONE}).read
			forecast = JSON.parse(output)


			wind_spd = forecast["currently"]["windSpeed"]
			wind_spd = to_kph(wind_spd.to_f).to_s
			wind_brg = forecast["currently"]["windBearing"]
			rain_val = forecast["currently"]["precipIntensity"]
			rain_val = to_mmeter(rain_val.to_f).to_s
			dew_val = forecast["currently"]["dewPoint"]
			dew_val = to_celsius(dew_val)
			temp = forecast["currently"]["temperature"]
			icon = forecast["currently"]["icon"]
			temp = to_celsius(temp.to_f).to_s

			@w = Wind.create(speed: wind_spd, bearing: wind_brg)
			@r = Rain.create(amount: rain_val)
			@d = Dew.create(dew_point: dew_val)
			@t = Temperature.create(temp: temp, icon: icon)
				#if (station.days.where(date: TODAY).empty?)
				#	@today = station.days.create(date: TODAY)
				#	@today.readerbs.create(time: t, wind_id: @w.id, temperature_id: @t.id, rain_id: @r.id, dew_id:@d.id)
				#else
				#	@today = station.days.where(date: TODAY)
				#	@today.readerbs.create(time: t, wind_id: @w.id, temperature_id: @t.id, rain_id: @r.id, dew_id:@d.id)
				#end
			station.readerbs.create(time: t, wind_id: @w.id, temperature_id: @t.id, rain_id: @r.id, dew_id:@d.id)

		end	
	end

	desc "scrape data from http://www.bom.gov.au/vic/observations/vicall.shtml"
	task :bomu, [:name] => :environment do |t, args|

		DIG_REG = /\d/
		URLL = 'http://www.bom.gov.au/vic/observations/vicall.shtml'
		doc = Nokogiri::HTML(open(URLL))

	        part = 'http://www.bom.gov.au'
		newurls = []
		names = []
		doc.css("tr.rowleftcolumn").each do |data|
			newurls << part + data.css('a')[0]['href']
    		names << data.at_css("th").text
		end

		#(0...1).each do |i|
		n = args.to_s
		if( names.index(n) != nil)
			i = names.index(n)
			puts i
			@station = Station.find_by(name: names[i])
			if(@station)
    			doc = Nokogiri::HTML(open(newurls[i]))
    			doc.css("tr.rowleftcolumn").each do |data|
	        		time = data.at_css("td:nth-child(1)").text
	        		timen = Time.now()
	        		if(time[0..1].to_i == timen.day)
	        			time = format_time(time,timen)
	        			@valid = @station.readers.select {|p| p.time == time}
	        			if(@valid.length == 0)
		        			@reading = @station.readers.create(time: time)
		        			puts time
		        			rain_val = data.at_css("td:nth-child(14)").text
		        			temp = data.at_css("td:nth-child(2)").text
		        			wind_spd = data.at_css("td:nth-child(8)").text
		        			wind_dir = data.at_css("td:nth-child(7)").text
		        			@w = Wind.create(speed: wind_spd, direction: wind_dir)
							@reading.wind_id = @w.id
							if DIG_REG.match(rain_val)
								@r = Rain.create(amount: rain_val)
								@reading.rain_id = @r.id
							end
							if DIG_REG.match(temp)
								@t = Temperature.create(temp: temp)
								@reading.temperature_id = @t.id
							end
							@reading.save
	    	        	end
	    	        end
	    	    end
    	    end
    	end
		#end
	end

	desc "scrape data of forecast.io"
	task :forecastu, [:name] => :environment do |t, args|

		# Define the URL
		API_KEY = '58fca16add9ba66cbb0a07e88a43a8d2'
		BASE_URL = 'https://api.forecast.io/forecast'

		n = args.to_s
		t = Time.now()
		@station = Station.find_by(name: n)
		
		if(@station)
			lat = @station.latitude.to_s
			long = @station.longitude.to_s
			lat_long= lat + ',' + long

			full_url = "#{BASE_URL}/#{API_KEY}/#{lat_long}"
			output = open(full_url, {ssl_verify_mode: OpenSSL::SSL::VERIFY_NONE}).read
			forecast = JSON.parse(output)


			wind_spd = forecast["currently"]["windSpeed"]
			wind_spd = to_kph(wind_spd.to_f).to_s
			wind_brg = forecast["currently"]["windBearing"]
			rain_val = forecast["currently"]["precipIntensity"]
			rain_val = to_mmeter(rain_val.to_f).to_s
			temp = forecast["currently"]["temperature"]
			icon = forecast["currently"]["icon"]
			temp = to_celsius(temp.to_f).to_s

			@w = Wind.create(speed: wind_spd, bearing: wind_brg)
			@r = Rain.create(amount: rain_val)
			@t = Temperature.create(temp: temp, icon: icon)
			#if (station.days.where(date: TODAY).empty?)
			#	@today = station.days.create(date: TODAY)
			#	@today.readerbs.create(time: t, wind_id: @w.id, temperature_id: @t.id, rain_id: @r.id, dew_id:@d.id)
			#else
			#	@today = station.days.where(date: TODAY)
			#	@today.readerbs.create(time: t, wind_id: @w.id, temperature_id: @t.id, rain_id: @r.id, dew_id:@d.id)
			#end
			@station.readerbs.create(time: t, wind_id: @w.id, temperature_id: @t.id, rain_id: @r.id)

            @valid = @station.readerbs.select {|p| p.time.hour == t.hour}
			if(@valid.length <= 1)
				(1..7).each do |i|
					tt = t - 3600*24*i 
					

					lat_long_time = lat_long + ',' + "#{tt.year}-"+"#{tt.month}".rjust(2, padstr='0')+'-'+"#{tt.day}".rjust(2, padstr='0')+'T'+"#{tt.hour}".rjust(2, padstr='0')+':'+"#{tt.min}".rjust(2, padstr='0')+':'+"#{tt.sec}".rjust(2, padstr='0')

					full_url = "#{BASE_URL}/#{API_KEY}/#{lat_long_time}"
					puts full_url
					output = open(full_url, {ssl_verify_mode: OpenSSL::SSL::VERIFY_NONE}).read
					forecast = JSON.parse(output)
					wind_spd = forecast["currently"]["windSpeed"]
				    wind_spd = to_kph(wind_spd.to_f).to_s
				    wind_brg = forecast["currently"]["windBearing"]
					rain_val = forecast["currently"]["precipIntensity"]
				    rain_val = to_mmeter(rain_val.to_f).to_s
					temp = forecast["currently"]["temperature"]
					temp = to_celsius(temp.to_f).to_s

					@w = Wind.create(speed: wind_spd, bearing: wind_brg)
					@t = Temperature.create(temp: temp)
					@r = Rain.create(amount: rain_val)
					#if (station.days.where(date: TODAY).empty?)
					#	@today = station.days.create(date: TODAY)
					#	@today.readerbs.create(time: t, wind_id: @w.id, temperature_id: @t.id, rain_id: @r.id, dew_id:@d.id)
					#else
					#	@today = station.days.where(date: TODAY)
					#	@today.readerbs.create(time: t, wind_id: @w.id, temperature_id: @t.id, rain_id: @r.id, dew_id:@d.id)
					#end
					@station.readerbs.create(time: tt, wind_id: @w.id, temperature_id: @t.id, rain_id: @r.id)
				    
				end
			end
		end
	end

	desc "scrape data from BoM and forecast"
	task :all => [:bom, :forecast] do
	end


	def to_celsius(f)
		return ((f-32)/1.8).round(2)
	end

	def to_mmeter(i)
		return (i*25.4*24).round(2)
	end

	def to_kph(mph)
		return (mph*1.609344).round(2)
	end

	def format_time(time,timenow)
    		hour = 0
    		if time[3..4].to_i < 12 
        		hour += time[3..4].to_i
    		end
    		if time[8..9] == "pm"
			hour += 12
	    	end
    		datetime = Time.new(timenow.year,timenow.month,timenow.day,hour,time[6..7].to_i,0)
    		return datetime
	end
end
